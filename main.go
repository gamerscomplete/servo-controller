package main

import (
        "fmt"
        "log"

        "periph.io/x/periph/conn/i2c/i2creg"
        "periph.io/x/periph/conn/i2c"
        "periph.io/x/periph/host"
)

const (
	PCA9685_PRESCALE_MIN = 3
	PCA9685_PRESCALE_MAX = 255
	FREQUENCY_OSCILLATOR = 25000000
)

func main() {

	// Make sure periph is initialized.
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	// Use i2creg I²C bus registry to find the first available I²C bus.
	b, err := i2creg.Open("")
	if err != nil {
		log.Fatal(err)
	}
	defer b.Close()


	// Dev is a valid conn.Conn.
	d := &i2c.Dev{Addr: 23, Bus: b}

	prescale := ((FREQUENCY_OSCILLATOR / (1000 * 4096)) + 0.5) -1
	if prescale < PCA9685_PRESCALE_MIN {
		prescale = PCA9685_PRESCALE_MIN
	} else if prescale > PCA9685_PRESCALE_MAX {
		prescale = PCA9685_PRESCALE_MAX
	}

	fmt.Println("final prescale: ", prescale)

	//prescale is 5

	// Send a command 0x10 and expect a 5 bytes reply.
	write := []byte{0xFE, 0x05}
	_, err := d.Write(write)
	if err != nil {
		fmt.Println("Failed to write:", err)
	}
/*
	read := make([]byte, 5)
	if err := d.Tx(write, read); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%v\n", read)
*/
}
